require('dotenv').config();
const express = require('express');
const dataBase = require('./mongo/connection');
const cors = require('cors');
const morgan = require('morgan');
const path = require("path");
const errorHandler = require('./ultility/error');
const v1Router = require('./v1/router/index');
const { io } = require('./v1/services/chat');

const app = express();
const server = require('http').createServer(app);

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(morgan("dev"));
app.use("/image", express.static(path.join(__dirname, "public/image")));

// Routes
app.use('/v1', v1Router);

// Handle Not Found Routes
app.use("*", (req, res, next) => {
  try {
    throw new Error("Routes Not Found");
  } catch (error) {
    next(error);
  }
});

// Error Handler Middleware
app.use(errorHandler)

// Socket.io Integration
io.attach(server);

const port = process.env.PORT || 7000;
server.listen(port, () => {
  console.clear();
  dataBase();
  console.log(`Server is running on port ${port}`);
  console.log(`URL: ${process.env.BASE_URL}`);
});
