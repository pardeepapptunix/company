const mongoose = require('mongoose');
const postSchema= new mongoose.Schema({
    userId:{
        type:mongoose.Types.ObjectId,
        ref:"User"
    },
    text:{
        type:String,
        default:""
    },
    video:{
        type:String,
        default:""
    },
    image:{
        type:String,
        default:""
    },
    likeCount:{
        type:Number,
        default:0
    },
    commentCount:{
        type:Number,
        default:0
    },
    isDeleted:{
        type:Boolean,
        default:false
    },
},{timestamps:true})
module.exports=mongoose.model("Post",postSchema,"Post")