const { string } = require('joi');
const mongoose = require('mongoose');
const otpSchema = new mongoose.Schema({
    userId:{
        type:mongoose.Types.ObjectId,
        ref:"User"
    },
    email:{
        type:String,
    },
    phone:{
        type:String,
    },
    countryCode:{
        type:String,
    },
    otp:{
        type:Number,
        required:true
    },
    expiredAt:{
        type:Date,
    },
    isDeleted:{
        type:Boolean,
        default:false
    },
    

},{timestamps:true})
module.exports=mongoose.model('Otp',otpSchema,'Otp');