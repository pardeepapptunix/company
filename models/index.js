module.exports={
    User:require('./user'),
    Otp:require('./otp'),
    Post:require('./post'),
    Like:require('./like'),
    Comment:require('./comment'),
    Friend:require('./friends'),
    Chat:require('./chat'),
}