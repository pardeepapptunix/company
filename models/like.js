const mongoose = require('mongoose');
const likeSchema= new mongoose.Schema({
    userId:{
        type:mongoose.Types.ObjectId,
        ref:"User"
    },
    postId:{
        type:mongoose.Types.ObjectId,
        ref:"Post"
    },
    isLike:{
        type:Boolean,
        default:true
    },
},{timestamps:true})
    module.exports=mongoose.model("Like",likeSchema,"Like")