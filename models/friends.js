const mongoose = require('mongoose');

const friendSchema = new mongoose.Schema({
  user1: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  user2: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  status:{
    type:Number,
    enum:[0,1,2,3], // Status values: 0 - Pending, 1 - Accepted, 2 - Rejected,3 - Cancel
    default:0
},
isDeleted:{
    type:Boolean,
    default:false
},

isBlocked:{
    type:Boolean,
    default:false
},
isAlreadyMyFriend:{
    type:Boolean,
    default:false
}
  
},{timestamp:true});

module.exports = mongoose.model('Friend', friendSchema,"Friend");
