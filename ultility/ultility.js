const bcrypt= require('bcrypt');
const jwt = require('jsonwebtoken')
const crypto =require('crypto');
const config  = require('config');
const { token } = require('morgan');

module.exports={
    isEmail:(email)=>{
        let regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(String(email).toLowerCase())
    },
    isPhone:(phone)=>{
        let regex = /[0-9 -()+]+$/;
        return regex.test(phone)
    },
    generateRandomNumber:(n)=>{
        return Number(Math.floor(Math.random() * Math.pow(10,n)));
    },
    jwtSign:async(...data)=>{
       return await jwt.sign(...data,config.get("jsonToken.secret"),{expiresIn:config.get("jsonToken.expiresIn")})
    },
    jwtVerify:async(token)=>{
        return await jwt.verify(token,config.get("jsonToken.secret"),)
    },
    hashPassword:async(password)=>{
        return await bcrypt.hash(password,10)
    },
    comaprePassword:async(password,hash)=>{
        return await bcrypt.compare(password,hash)
    }
}