function errorHandler(err, req, res, next) {
    console.log("dsfsd",err);
    switch (true) {
        case typeof err === 'string':
            const is404 = err.toLowerCase().endsWith('not found');
            const statusCode = is404 ? 404 : 400;
            return res.status(statusCode).json({ status: statusCode, message: err, data: null });
        case typeof err === 'object' && err.name === 'CastError':
            return res.status(404).json({ status: 404, message: 'Not Found', data: null });
        case typeof err === 'object' && typeof err.message === 'string' && err.message.toLowerCase().endsWith('jwt expired'):
            return res.status(401).json({ status: 401, message: err.message, data: null });
        case typeof err === 'object' && typeof err.message === 'string' && err.message.toLowerCase().endsWith('jwt malformed'):
            return res.status(401).json({ status: 401, message: err.message, data: null });
        case typeof err === 'object' && typeof err.message === 'string':
            const value = err.message.toLowerCase().endsWith('not found');
            const code = value ? 404 : 400;
            return res.status(code).json({ status: code, message: err.message, data: null });
        case err.name === 'UnauthorizedError':
            return res.status(401).json({ status: 401, message: 'Unauthorized', data: null });
        default:
            return res.status(500).json({ status: 500, message: err.message || err, data: null });
    }
}

module.exports = errorHandler;
