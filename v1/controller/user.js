const ultility = require("../../ultility/ultility");
const service = require("../services/index");
const validate = require("../validate/index");

async function signUp(req, res, next) {
  try {
    await validate.user.validateSignUp(req);
    let user = await service.user.signUp(req);
    return res.status(201).json({user,message:"register successfully"})
  } catch (error) {
    next(error);
  }
};
async function verifyOtp(req, res, next) {
  try {
    await validate.user.validateVerifyOtp(req);
    let user = await service.user.verifyOtp(req);
    return res.status(201).json({data:user,message:"otpverify success"})
  } catch (error) {
    next(error);
  }
}
async function login(req, res, next) {
  try {
    await validate.user.validateLogin(req);
    let user = await service.user.login(req);
    return res.status(201).json({user,message:"login successfully"})
  } catch (error) {
    next(error);
  }
}
async function forgetPassword(req, res, next) {
  try {
    await validate.user.validateForgetPassword(req);
    let user = await service.user.forgetPassword(req);
    return res.status(201).json({data:user,message:"otp send successfully"})
  } catch (error) {
    next(error);
  }
};
async function resetPassword(req, res, next) {
  try {
    await validate.user.validateResetPassword(req);
    let user = await service.user.resetPassword(req);
    return res.status(201).json({data:user,message:"password set succussfully"})
  } catch (error) {
    next(error);
  }
};
async function getProfile(req, res, next) {
  try {
    let user = await service.user.getProfile(req);
    return res.status(201).json({data:user,message:"fetch succussfully"})
  } catch (error) {
    next(error);
  }
};
async function updateProfile(req, res, next) {
  try {
    await validate.user.validateUpdateProfile(req);
    let user = await service.user.updateProfile(req);
    return res.status(201).json({data:user,message:"update succussfully"})
  } catch (error) {
    next(error);
  }
};
async function changePassword(req, res, next) {
  try {
    await validate.user.validateChangePassword(req);
    let user = await service.user.changePassword(req);
    return res.status(201).json({data:user,message:"Password change successfully"})
  } catch (error) {
    next(error);
  }
};
async function logout(req, res, next) {
  try {
    let user = await service.user.logout(req);
    return res.status(201).json({data:user,message:"logout succussfully"})
  } catch (error) {
    next(error);
  }
};
async function profileImage(req, res, next) {
  try {
          return res.status(200).json({imageUrl:`localhost:3690/user/profile/image/${req.file.filename}`}) 
  
  } catch (error) {
    next(error);
  }
}
// upload  image video pdf audio
async function upload (req, res, next){
  try {
      if (req && req.file.filename == undefined) throw new Error("No file uploaded");

      console.log(req.file)
      res.status(200).json({
          success: true,
          message: 'image upload successfully',
          url: `${process.env.BASE_URL ? process.env.BASE_URL:"https://company-g42t.onrender.com"}${req.file.destination.split('public')[1]}/${req.file.filename.trim().replace(/\s+/g, '_')}`
      });
  } catch (error) {
      next(error);
  }
};

async function postImage(req, res, next) {
  try {
          return res.status(200).json({imageUrl:`localhost:3690/post/image/${req.file.filename}`}) 
  
  } catch (error) {
    next(error);
  }
};
async function addPost(req, res, next) {
  try {
    await validate.user.validatePost(req);
    let post = await service.user.addPost(req);
    return res.status(201).json({post,message:"post create succussfully"})
  } catch (error) {
    next(error);
  }
};
async function getOneUserAllPost(req, res, next) {
  try {
    let {post,count} = await service.user.getOneUserAllPost(req);
    return res.status(201).json({count,message:"fetch succussfully",post})
  } catch (error) {
    next(error);
  }
};
async function getAllPost(req, res, next) {
  try {
    let {post,count} = await service.user.getALLPost(req);
    return res.status(201).json({count,message:"fetch succussfully",post})
  } catch (error) {
    next(error);
  }
};
async function editPost(req, res, next) {
  try {
    await validate.user.validateUpdatePost(req);
    let post = await service.user.editPost(req);
    return res.status(201).json({message:"Update succussfully",post})
  } catch (error) {
    next(error);
  }
};
async function deletePost(req, res, next) {
  try {   
    let post = await service.user.deletePost(req);
    return res.status(201).json({message:"delete succussfully",post})
  } catch (error) {
    next(error);
  }
};
async function likeDislike(req, res, next) {
  try {   
    let like = await service.user.likeDislike(req);
    return res.status(201).json({message:like.isLike=true?"like succussfully":"diLike succussfully",like})
  } catch (error) {
    next(error);
  }
};
async function getLikesByPostId(req, res, next) {
  try {   
    let {like,count} = await service.user.getLikesByPostId(req);
    return res.status(201).json({count,message:"like succussfully",like})
  } catch (error) {
    next(error);
  }
};
async function addComment(req, res, next) {
  try {
    await validate.user.validateComment(req);
    let post = await service.user.comment(req);
    return res.status(201).json({post,message:"comments add on post sccuessFully"})
  } catch (error) {
    next(error);
  }
};
async function getAllCommentsByPostId(req, res, next) {
  try {
    let comment= await service.user.getCommentsByPostId(req);
    return res.status(201).json({count:comment.length,message:comment.length===0?"No comment found":"fetch succussfully",comment})
  } catch (error) {
    next(error);
  }
};
async function getCommentsById(req, res, next) {
  try {
    let comment= await service.user.getCommentById(req);
    return res.status(201).json({message:"fetch succussfully",comment})
  } catch (error) {
    next(error);
  }
};
async function editComment(req, res, next) {
  try {
    let comment= await service.user.editComment(req);
    return res.status(201).json({message:"update succussfully",comment})
  } catch (error) {
    next(error);
  }
};
async function deleteComment(req, res, next) {
  try {
    let post = await service.user.deleteComment(req);
    return res.status(201).json({post,message:"comments delete on post sccuessFully"})
  } catch (error) {
    next(error);
  }
};
async function sendFriendRequest(req, res, next) {
  try {
    await validate.user.validateSendFriendRequest(req);
    let {friends,fds} = await service.user.sendFriendRequest(req);
    return res.status(201).json({friends,message:"comments add on post sccuessFully"})
  } catch (error) {
    next(error);
  }
};
async function getAllFriendReqest(req, res, next) {
  try {
    let {count,reqest} = await service.user.getAllFriendReqest(req);
    return res.status(201).json({count,message:"fetch succussfully",reqest})
  } catch (error) {
    next(error);
  }
};
module.exports = {
  signUp,
  verifyOtp,
  login,
  forgetPassword,
  resetPassword,
  getProfile,
  updateProfile,
  changePassword,
  logout,
  profileImage,
  postImage,
  addPost,
  getAllPost,
  getOneUserAllPost,
  editPost,
  deletePost,
  likeDislike,
  getLikesByPostId,
  addComment,
  getAllCommentsByPostId,
  getCommentsById,
  editComment,
  deleteComment,
  sendFriendRequest,
  upload
};
