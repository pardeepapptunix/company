const joi = require("joi");
const validateSchema = async (input, schema) => {
  try {
    const { error, value } = schema.validate(input);
    if (error){
    console.log("error in validation",error.message)
      throw error.details ? error.details[0].message.replace(/['"]+/g, "") : "";
    }
    else return false;
  } catch (error) {
    throw error;
  }
};

const validateSignUp = async function (req, property = "body") {
  let schema = {};
  schema = joi.object({
    email: joi.string().email().optional().lowercase(),
    phone: joi.string().optional(),
    countryCode: joi.string().optional(),
    firstName: joi.string().optional(),
    lastName: joi.string().optional().lowercase(),
    userName: joi.string().optional().lowercase(),
    password: joi.string().required().min(8),
    image: joi.string().optional(),
    confirmPassword: joi.string().optional(),
  });
  await validateSchema(req[property], schema);
};
const validateVerifyOtp = async function (req, property = "body") {
  let schema = {};
  schema = joi.object({
    email: joi.string().optional(),
    phone: joi.string().optional(),
    countryCode: joi.string().optional(),
    deviceType: joi.string().valid("web", "iso", "andorid").optional(),
    deviceToken: joi.string().optional(),
    otp: joi.number().optional(),
  });
  await validateSchema(req[property], schema);
};
const validateLogin = async function (req, property = "body") {
  let schema = {};
  schema = joi.object({
    email: joi.string().optional(),
    password: joi.string().required(),
    phone: joi.string().optional(),
    countryCode: joi.string().optional(),
    deviceType: joi.string().valid("web", "iso", "andorid").optional(),
    deviceToken: joi.string().optional(),
  });
  await validateSchema(req[property], schema);
};
const validateForgetPassword = async function (req, property = "body") {
  let schema = {};
  schema = joi.object({
    email: joi.string().optional(),
    phone: joi.string().optional(),
    countryCode: joi.string().optional(),
  });
  await validateSchema(req[property], schema);
};
const validateResetPassword = async function (req, property = "body") {
  let schema = {};
  schema = joi.object({
    password: joi.string().required(),
  });
  await validateSchema(req[property], schema);
};
const validateUpdateProfile = async function (req, property = "body") {
  let schema = {};
  schema = joi.object({
    email: joi.string().optional().lowercase(),
    phone: joi.string().optional(),
    countryCode: joi.string().optional(),
    firstName: joi.string().optional(),
    lastName: joi.string().optional().lowercase(),
    userName: joi.string().optional().lowercase(),
    image: joi.string().optional(),
  });
  await validateSchema(req[property], schema);
};
const validateChangePassword = async function (req, property = "body") {
  let schema = {};
  schema = joi.object({
    oldPassword: joi.string().required(),
    newPassword: joi.string().required(),

  });
  await validateSchema(req[property], schema);
};
const validatePost = async function (req, property = "body") {
  let schema = {};
  if (!req.body || Object.keys(req.body).length === 0) {
    throw new Error("Please enter at least one field ");
  }
  schema = joi.object({
    image: joi.string().optional(),
    text: joi.string().optional(),
    video: joi.string().optional(),
  });
  await validateSchema(req[property], schema);
};
const validateUpdatePost = async function (req, property = "body") {
  let schema = {};
  if (!req.body || Object.keys(req.body).length === 0) {
    throw new Error("Please enter at least one field ");
  }
  schema = joi.object({
    text: joi.string().optional(),
  });
  await validateSchema(req[property], schema);
};
const validateComment = async function (req, property = "body") {
  let schema = {};
  schema = joi.object({
    comment: joi.string().required(),
    postId: joi.string().required(),
  });
  await validateSchema(req[property], schema);
};
const validateSendFriendRequest = async function (req, property = "body") {
  let schema = {};
  schema = joi.object({
    type: joi.string().optional()
  });
  await validateSchema(req[property], schema);
};
module.exports = {
  validateSignUp,
  validateVerifyOtp,
  validateLogin,
  validateForgetPassword,
  validateResetPassword,
  validateUpdateProfile,
  validateChangePassword,
  validatePost,
  validateUpdatePost,
  validateComment,
};
