const Utility = require("../../ultility/ultility");
const Model = require("../../models/index");
const userAuth = async (req, res, next) => {
  try {
    if (req && req.user && req.user.guestMode) {
      next();
    } else if (req && (req.headers.authorization || req.headers.Authorization)) {
      const accessTokenFull = req.headers.authorization
        ? req.headers.authorization
        : req.headers.Authorization;
      let accessToken = "";
      if (accessTokenFull.startsWith("Bearer")) {
        accessToken = accessTokenFull.substr("Bearer".length + 1);
      } else {
        const parts = accessTokenFull.split(" ");
        accessToken = parts[0];
      }
      const decodeData = await Utility.jwtVerify(accessToken);
      //   console.log("sddf", decodeData, "dddd");
      if (!decodeData) throw new Error("invaild data");
      let userData;
      if (decodeData.jti) {
        userData = await Model.User.findOne({
          _id: decodeData.id,
          //   isBlocked: false,
          isDeleted: false,
          jti: decodeData.jti,
        })
          .lean()
          .exec();
        // console.log(userData,"gsajhgjgjhsagjh")
      } else {
        // console.log("inside else");
        userData = await Model.User.findOne({ _id: decodeData.id })
          .lean()
          .exec();
      }
      if (userData) {
        req.user = userData;
        req.user.userType = "USER";
        next();
      } else {
        return res.status(401).json({ message: " invaild token " });
      }
    } else {
      return res.status(401).json({ message: "token is missing" });
    }
  } catch (error) {
    console.log(error.message);
    next(error);
  }
};
module.exports = {
  userAuth,
};
