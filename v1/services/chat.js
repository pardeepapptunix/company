const {Server} = require("socket.io");
const Model = require('../../models/index');
const ObjectId = require('mongoose').Types.ObjectId;
const Utility= require('../../ultility/ultility');
const io = new Server()
let users = {};
let usersSockets = {};

io.use(async (socket, next) => {
    if (socket.handshake.query.token) {
    //   console.log(socket.handshake.query.token);
      let decoded = await Utility.jwtVerify(socket.handshake.query.token);
      console.log("user ", JSON.stringify(decoded));
      if (!decoded) return next(new Error("Authentication error"));
      else {
        users[String(socket.id)] = decoded.id;
        usersSockets[decoded.id] = socket;
        console.log("Connected socket email",users, decoded.email);
        socket.join(String(decoded.id));
        next();
      }
    } else {
      next(new Error("Authentication error"));
    }
  }).on("connection", function (socket) {
  
   // one to one chat sockets
   socket.on("connectToChat", async (data) => {
      const sender =users[String(socket.id)];
      const receiver = data.receiver;
      let  sortedIds = [sender, receiver].sort();
      sortedIds=sortedIds.join('_').slice(0,5 )
      const roomId = "room" + sortedIds;
      
      let email = await Model.User.findOne({ _id: new ObjectId(sender),isblocked:false},{_id:0,email:1})
      socket.join(roomId);
      io.to(roomId).emit("connectToChatOk", { status: 200, message: `chat room successfully joined`,roomId ,email});
   });
  
    socket.on("disconnect", () => {
      console.log("Disconnected:");
    });
    socket.on("sendMessage", async (data) => {
      console.log(data)
      data.sender =users[String(socket.id)];
      data.receiver;
       await Model.Chat.create(data)
      // console.log(message, roomId)
      socket.to(data.roomId).emit("recivedMessage", { message: data.message });

  });

  })




exports.io = io;