const ultility = require("../../ultility/ultility");
const otp = require("./otp");
const Model = require("../../models/index");
const ObjectId = require("mongoose").Types.ObjectId;

async function signUp(req) {
  let data = req.body;
  let user;
  if (ultility.isEmail(data.email)) {
    data.email = data.email.toLowerCase();
  } else if (ultility.isPhone(data.phone)) {
    data.phone = data.phone;
  } else {
    throw new Error("invaild data");
  }
  user = await Model.User.findOne({ email: data.email ,isDeleted: false,isEmailverify: true});  
  if (user) throw new Error("email already register");
  user = await Model.User.findOne({ phone: data.phone,countryCode:data.countryCode ,isDeleted: false,isEmailverify: true});  
  if (user) throw new Error("phone already register");
  user = await Model.User.deleteMany({
    $or: [{ phone: data.phone, email: data.email }],
    isDeleted: false,
  });
  data.password = await ultility.hashPassword(data.password);
  user = await Model.User.create(data);
  if (data.email) {
    await otp.sendEmailOtp(data.email, user);
  }
  if (data.phone) {
    await otp.sendPhoneOtp(data.phone, data.countryCode, user);
  }
  if(data.password){
    delete data.password
  }
  console.log(data);
  return data;
}
async function verifyOtp(req) {
  let data = req.body;
  if (ultility.isEmail(data.email)) {
    data.email = data.email.toLowerCase();
  } else if (ultility.isPhone(data.phone)) {
    data.phone = data.phone;
  } else {
    throw new Error("invaild data");
  }
  let user = await Model.User.findOne({
    $or: [{ email: data.email }, { phone: data.phone }],
    isDeleted: false,
  });
  if (!user) throw new Error("User not found");
  let otp = await Model.Otp.findOne({
    $or: [{ email: data.email }, { phone: data.phone }],
    isDeleted: false,
  });

  if (!otp) throw new Error("Otp not found");
  if (otp.expiredAt < Date.now()) throw new Error("otp is expired");
  if (otp.otp != data.otp) throw new Error("otp is incorrect");
  if (otp.email) {
    await Model.User.findOneAndUpdate(
      { email: otp.email },
      { isEmailverify: true }
    );
  }
  if (otp.phone) {
    await Model.User.findOneAndUpdate(
      { email: otp.phone },
      { isPhoneverify: true }
    );
  }
  let qty = {
    deviceType: data.deviceType,
    deviceToken: data.deviceToken,
  };
  let jti = await ultility.generateRandomNumber(20);
  qty.jti = jti;
  user = await Model.User.findByIdAndUpdate(
    { _id: user._id },
    { $set: qty }
  ).lean();
  user.token = await ultility.jwtSign({
    jti: jti,
    email: user.email,
    id: user._id,
  });
  user.tokenType = "Bearer";
  return user;
}
async function login(req) {
  let data = req.body;
  let payload = {};
  if (ultility.isEmail(data.email)) {
    payload.email = data.email.toLowerCase();
  } else if (ultility.isPhone(data.phone)) {
    payload.phone = data.phone;
    payload.countryCode = data.countryCode;
  } else {
    throw new Error("invaild data");
  }
  payload.isDeleted = false;
  payload.role = "user";
  let user = await Model.User.findOne(payload);
  if (!user) throw new Error(`Invaild credentails`);
  const match = await ultility.comaprePassword(data.password, user.password);
  if (!match) throw new Error(`Incorrect Password`);
  let qty = {
    deviceType: data.deviceType,
    deviceToken: data.deviceToken,
  };
  let jti = await ultility.generateRandomNumber(20);
  qty.jti = jti;
  user = await Model.User.findByIdAndUpdate(
    { _id: user._id },
    { $set: qty }
  ).lean();
  user.token = await ultility.jwtSign({
    jti: jti,
    email: user.email,
    id: user._id,
  });
  user.tokenType = "Bearer";
  console.log(" user", user);
  return user;
}
async function forgetPassword(req) {
  let data = req.body;
  let otp1;
  let user = await Model.User.findOne({
    $or: [{ email: data.email.toLowerCase() }, { phone: data.phone }],
    isDeleted: false,
  });

  if (ultility.isEmail(data.email)) {
    data.email = data.email.toLowerCase();
    console.log("", user, data);
    otp1 = await otp.sendEmailOtp(data.email, user);
  } else if (ultility.isPhone(data.phone)) {
    data.phone = data.phone;
    otp1 = await otp.sendPhoneOtp(data.phone, data.countryCode, user);
  } else {
    throw new Error("invaild data");
  }
  return otp1;
}
async function resetPassword(req) {
  if (!req.user) throw new Error("Unautherize user");
  let data = req.body;
  data.password = await ultility.hashPassword(data.password);
  let user = await Model.User.findOneAndUpdate(
    { _id: req.user._id, isDeleted: false },
    { $set: { password: data.password } }
  );
  // console.log(" user", user);
  return user;
}
async function getProfile(req) {
  if (!req.user) throw new Error("Unautherize user");
  let user = await Model.User.findOne({ _id: req.user._id, isDeleted: false });
  return user;
}
async function updateProfile(req) {
  if (!req.user) throw new Error("Unautherize user");
  if (!req && req.body) throw new Error(`Invaild credentails`);
  let data = req.body;
  if (data.email) {
    let user = await Model.User.findOne({ email: data.email });
    if (user) throw new Error("duplicate email");
  }
  if (data.phone) {
    user = await Model.User.findOne({ phone: data.phone });
    if (user) throw new Error("duplicate phone");
  }
  user = await Model.User.findOneAndUpdate(
    { _id: req.user._id, isDeleted: false },
    { $set: data },
    { new: true }
  );
  return user;
}
async function changePassword(req) {
  if (!req.user) throw new Error("Unautherize user");
  if (!req && req.body) throw new Error(`Invaild credentails`);
  let data = req.body;

  user = await Model.User.findOne(
    { _id: req.user._id, isDeleted: false },
    { password: 1 }
  );
  let match = await ultility.comaprePassword(data.oldPassword, user.password);
  if (!match) throw new Error("Incorrect password");
  let password = await ultility.hashPassword(data.newPassword);
  user = await Model.User.findOneAndUpdate(
    { _id: req.user._id, isDeleted: false },
    { $set: { password: password } },
    { new: true }
  );
  return user;
}
async function logout(req) {
  if (!req.user) throw new Error("Unautherize user");
  if (!req && req.body) throw new Error(`Invaild credentails`);
  user = await Model.User.findOneAndUpdate(
    { _id: req.user._id, isDeleted: false },
    { $set: { jti: null, deviceToken: null } },
    { new: true }
  );
  return user;
}
async function addPost(req) {
  if (!req.user) throw new Error("Unautherize user");
  if (!req && req.body == {}) throw new Error(`Invaild credentails`);
  let data = req.body;
  console.log(req.body, "sjcn");
  data.userId = new ObjectId(req.user._id);
  let post = await Model.Post.findOne({
    userId: new ObjectId(req.user._id),
    text: data.text,
    isDeleted: false,
    $or: [{ image: data.image }, { video: data.video }],
  });
  if (post) throw new Error(`Post already upload`);
  post = await Model.Post.create(data);
  return post;
}
async function getOneUserAllPost(req) {
  if (!req.user) throw new Error("Unautherize user");
  let page = req.query.page ? Number(req.query.page) : 1;
  let limit = req.query.limit ? Number(req.query.limit) : 10;
  let skip = Number((page - 1) * limit);
  let post;
  if (req.params.id) {
    post = await Model.Post.findOne({
      _id: new ObjectId(req.params.id),
      userId: new ObjectId(req.user._id),
      isDeleted: false,
    });
    if (!post) throw new Error(`Post no found`);
    return { post };
  }
  post = await Model.Post.find({
    userId: new ObjectId(req.user._id),
    isDeleted: false,
  })
    .skip(skip)
    .sort({ createdAt: -1 });
  if (post.length < 0) throw new Error(`Posts no found`);
  let count = await Model.Post.countDocuments({
    userId: new ObjectId(req.user._id),
    isDeleted: false,
  });

  return { post, count };
}
async function getALLPost(req) {
  if (!req.user) throw new Error("Unautherize user");
  let page = req.query.page ? Number(req.query.page) : 1;
  let limit = req.query.limit ? Number(req.query.limit) : 10;
  let skip = Number((page - 1) * limit);
  let post;
  post = await Model.Post.find({ isDeleted: false })
    .skip(skip)
    .sort({ createdAt: -1 })
    .populate("userId");
  if (post.length < 0) throw new Error(`Posts no found`);
  let count = await Model.Post.countDocuments({ isDeleted: false });

  return { post, count };
}
async function editPost(req) {
  console.log("hello");
  if (!req.user) throw new Error("Unautherize user");
  if (!req && req.params) throw new Error(`Invaild credentails`);
  let data = req.body;
  let post = await Model.Post.findOne({
    _id: new ObjectId(req.params.id),
    userId: new ObjectId(req.user._id),
    isDeleted: false,
  });
  if (!post) throw new Error(`Post no found`);
  post = await Model.Post.findOneAndUpdate(
    { _id: new ObjectId(req.params.id) },
    { $set: data },
    { new: true }
  );

  return post;
}
async function deletePost(req) {
  if (!req.user) throw new Error("Unautherize user");
  if (!req && req.params) throw new Error(`Invaild credentails`);
  let post = await Model.Post.findOne({
    _id: new ObjectId(req.params.id),
    userId: new ObjectId(req.user._id),
    isDeleted: false,
  });
  if (!post) throw new Error(`Post already delete`);
  post = await Model.Post.findByIdAndUpdate(
    new ObjectId(req.params.id),
    { isDeleted: true },
    { new: true }
  );
  return post;
}
async function likeDislike(req) {
  const { user, params } = req;

  if (!user) throw new Error("Unauthorized user");
  if (!params) throw new Error("Invalid credentials");
  const postId = new ObjectId(params.id);
  const userId = new ObjectId(user._id);

  let post = await Model.Post.findOne({
    _id: postId,
    userId: userId,
    isDeleted: false,
  });
  if (!post) {
    throw new Error("Post not found");
  }
  let like = await Model.Like.findOne({ postId, userId });
  if (like && like.isLike === true) {
    await Model.Like.findOneAndUpdate({ postId, userId }, { isLike: false });
    post.likeCount -= 1;
  } else if (like && like.isLike === false) {
    await Model.Like.findOneAndUpdate({ postId, userId }, { isLike: true });
    post.likeCount += 1;
  } else {
    await Model.Like.create({ userId, postId, isLike: true });
    post.likeCount += 1;
  }
  await post.save();
  like = await Model.Like.findOne({ userId, postId });

  return like;
}
async function getLikesByPostId(req) {
  if (!req.user) throw new Error("Unautherize user");
  let page = req.query.page ? Number(req.query.page) : 1;
  let limit = req.query.limit ? Number(req.query.limit) : 10;
  let skip = Number((page - 1) * limit);
  let like;
  let count;
  const params = req.params;
  if (!params) throw new Error("Invalid credentials");

  like = await Model.Like.find({postId: new ObjectId(params.id),isLike: true,}).sort({ createdAt: -1 }).skip(skip).limit(limit).populate("userId");
  if (!like) throw new Error("post have no like");
  count = await Model.Like.countDocuments({
    postId: new ObjectId(params.id),
    isLike: true,
  });
  return { like, count };
};
async function comment(req) {
  const { user, body } = req;

  if (!user) throw new Error("Unauthorized user");
  if (!body) throw new Error("Invalid credentials");
  const postId = new ObjectId(body.postId);
  const userId = new ObjectId(user._id);
  let post = await Model.Post.findOne({ _id: postId, isDeleted: false });
  if (!post) throw new Error("Post not found");
  comment = await Model.Comment.create({
    userId: userId,
    postId: postId,
    comment: body.comment,
  });
  post.commentCount += 1;
  await post.save();
  return comment;
};
async function getCommentsByPostId(req) {
  if (!req.user) throw new Error("Unautherize user");
  let page = req.query.page ? Number(req.query.page) : 1;
  let limit = req.query.limit ? Number(req.query.limit) : 10;
  let skip = Number((page - 1) * limit);
  let comment;
  let count;
  const params = req.params;
  if (!params) throw new Error("Invalid credentials");

  comment = await Model.Comment.find({
    postId: new ObjectId(params.id),
    isDeleted: false,
  })
    .sort({ createdAt: -1 })
    .skip(skip)
    .populate("userId");
  console.log(comment, ".........", comment.length);
  count = await Model.Comment.countDocuments({
    postId: new ObjectId(params.id),
    isDeleted: false,
  });
  return comment;
};
async function getCommentById(req) {
  if (!req.user) throw new Error("Unautherize user");
  let comment;
  if (req.params.id) {
    comment = await Model.Comment.findOne({
      _id: new ObjectId(req.params.id),
      isDeleted: false,
    }).populate("userId postId");
    if (!comment) throw new Error(`Post no found`);
  }
  return comment;
};
async function editComment(req) {
  const { user, body, params } = req;
  if (!user) throw new Error("Unauthorized user");
  if (!body) throw new Error("Invalid credentials");
  if (!params) throw new Error("Invalid credentials");
  const _id = new ObjectId(params.id);
  const userId = new ObjectId(user._id);
  let comment = await Model.Comment.findOne({
    _id: _id,
    userId: userId,
    isDeleted: false,
  });
  console.log("405", comment);
  if (!comment) throw new Error("comment not found");
  comment = await Model.Comment.findOneAndUpdate(
    { _id: _id },
    { comment: body.comment },
    { new: true }
  );
  return comment;
};
async function deleteComment(req) {
  const { user, params } = req;
  if (!user) throw new Error("Unauthorized user");
  if (!params) throw new Error("Invalid credentials");
  const _id = new ObjectId(params.id);
  const userId = new ObjectId(user.id);
  let comment = await Model.Comment.findOne({ _id: _id, isDeleted: false });
  post = await Model.Post.findOne({
    _id: new ObjectId(comment.postId),
    isDeleted: false,
  });
  if (!comment) throw new Error("comment  not found");
  if (!post) throw new Error("Post not found");
  // console.log("post",post.userId,"comment",comment.userId)
  if (comment.userId.equals(post.userId)) {
    comment = await Model.Comment.findOneAndUpdate(
      { _id: _id, isDeleted: false },
      { isDeleted: true },
      { new: true }
    );
  } else {
    comment = await Model.Comment.findOneAndUpdate(
      { _id: _id, userId: userId, isDeleted: false },
      { isDeleted: true },
      { new: true }
    );
  }
  post.commentCount -= 1;
  await post.save();
  return comment;
};
async function sendFriendRequest(req) {
  const { user, params } = req;

  if (!user) throw new Error("Unauthorized user");
  if (!params) throw new Error("Invalid credentials");
  const user1 = new ObjectId(user._id);
  const user2 = new ObjectId(params.id);
  let friend = await Model.Post.friend({ user1: user1,user2: user2,isBlocked:false}).populate("user2");
  if(req && req.body.type===3){
    friend.status=3;
    let fds = await friend.save();
    return fds;
  }
  if (friend && friend.status===0) throw new Error(`Already Send reqest  ${friend}`);
  else if(friend && friend.status===1)throw new Error(`Both are already friends  ${friend}`);
  else if(friend && (friend.status===2||friend.status===3)){
    friend.status=0;
    await friend.save();
  }else{
    friend = await Model.Comment.create({
      user1: user1,
      user2: user2
    });
  }
 
  return friend;
};
async function getAllFriendReqest(req) {
  if (!req.user) throw new Error("Unautherize user");
  let page = req.query.page ? Number(req.query.page) : 1;
  let limit = req.query.limit ? Number(req.query.limit) : 10;
  let skip = Number((page - 1) * limit);
  let reqest;
  let count;
  if(req && req.params.id){
    reqest = await Model.Friend.find({
      user1: new ObjectId(req.params.id),
      status:0,
      isBlocked: false
    })
    if(reqest.length<0)throw new Error("no friend found")
    return reqest
  }

  reqest = await Model.Comment.find({
    user2: new ObjectId(req.user._id),
    tatus:0,
    isBlocked: false
  }).sort({ createdAt: -1 }).skip(skip).limit(limit).populate("user1");
  console.log(reqest, ".........", reqest.length);
  count = await Model.Comment.countDocuments({
    user2: new ObjectId(req.user._id),
    tatus:0,
    isBlocked: false
  });
  return {count,reqest};
};
async function acceptOrRejectFriendRequest(req) {
  const { user, body } = req;

  if (!user) throw new Error("Unauthorized user");
  if (!body) throw new Error("Invalid credentials");
  const userId = new ObjectId(user._id);
  let data;
  let friend = await Model.Post.friend({ user2:userId,isBlocked:false,status:0}).populate("user1");
  if (body.type===1){
    friend.status=1;
    friend.isAlreadyMyFriend=true;
    data= await friend.save()
  }
  else if(body.type===2){
    friend.status=2;
    friend.isAlreadyMyFriend=false;
    data= await friend.save()
  }

 
  return data;
};

module.exports = {
  signUp,
  verifyOtp,
  login,
  forgetPassword,
  resetPassword,
  getProfile,
  updateProfile,
  changePassword,
  logout,
  addPost,
  getOneUserAllPost,
  getALLPost,
  editPost,
  deletePost,
  likeDislike,
  getLikesByPostId,
  comment,
  getCommentsByPostId,
  getCommentById,
  editComment,
  deleteComment,
  sendFriendRequest,
  getAllFriendReqest,
  acceptOrRejectFriendRequest
};
