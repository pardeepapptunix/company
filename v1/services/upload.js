const multer = require('multer');
const path = require('path'); // Add this line
const imageDir = 'public/image';

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        let dir;
        const extension = path.extname(file.originalname).toLowerCase();

        if (/^\.(png|jpg|jpeg|gif)$/.test(extension)) {
            dir = imageDir;
        } else {
            return cb(new Error('Invalid file type'));
        }

        cb(null, dir);
    },
    filename: (req, file, cb) => {
      const trimmedOriginalName = file.originalname.trim().replace(/\s+/g, '_'); 
        cb(null, Date.now() + '-' + trimmedOriginalName);
    },
});

const upload = multer({ storage: storage });

module.exports = upload;
