const Model = require("../../models/index");
const ultility = require("../../ultility/ultility");
const moment = require("moment");
async function sendEmailOtp(email, user) {
  let data = { email: email, userId: user._id };
  let OTP = await Model.Otp.findOne({ email: data.email});
  if (OTP) {
    OTP = await Model.Otp.deleteMany({ email: data.email});
  }
  // data.otp = await ultility.generateRandomNumber(4);
  data.otp=1234;
  data.expiredAt = moment().add(10, "minutes").toDate();
  OTP = await Model.Otp.create(data);
//   console.log("otp=======>", OTP);
  return OTP;
}
async function sendPhoneOtp(phone, countryCode, user) {
  let data = { phone: phone, userId: user._id,countryCode:countryCode };
  let OTP = await Model.Otp.findOne({ phone: data.phone});
  if (OTP) {
    OTP = await Model.Otp.deleteMany({ phone: data.phone});
  }
  data.otp = await ultility.generateRandomNumber(4);
  data.expiredAt = moment().add(10, "minutes").toDate();
  OTP = await Model.Otp.create(data);
//   console.log("otp=======>", OTP);
  return OTP;
}
module.exports = {
  sendEmailOtp,
  sendPhoneOtp,
};
