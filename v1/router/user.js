const express = require('express');
const controller = require('../controller/index');
const auth = require('../services/auth');
const upload = require('../services/upload');

const router = express.Router();

router.post('/signup',controller.user.signUp);
router.post('/verifyOtp',controller.user.verifyOtp);
router.post('/login',controller.user.login);
router.post('/forgetPassword',controller.user.forgetPassword);
router.post('/resetPassword',auth.userAuth,controller.user.resetPassword);
router.get('/getprofile',auth.userAuth,controller.user.getProfile);
router.put('/updateProfile',auth.userAuth,controller.user.updateProfile);
router.post('/changePassword',auth.userAuth,controller.user.changePassword);
router.post('/logout',auth.userAuth,controller.user.logout);
router.post('/upload',auth.userAuth,upload.single('file'), controller.user.upload);

//post
// router.post('/postImageUrl', auth.userAuth, upload.singlePostImage, controller.user.postImage);
router.post('/post',auth.userAuth,controller.user.addPost);
router.get('/getPostByUser',auth.userAuth,controller.user.getOneUserAllPost);
router.get('/getPostByPostId/:id',auth.userAuth,controller.user.getOneUserAllPost);
router.get('/getAllPost',auth.userAuth,controller.user.getAllPost);
router.put('/post/:id',auth.userAuth,controller.user.editPost);
router.delete('/post/:id',auth.userAuth,controller.user.deletePost);

//likeDislike
router.post('/likeDislike/:id',auth.userAuth,controller.user.likeDislike);
router.get('/getLikeOnPost/:id',auth.userAuth,controller.user.getLikesByPostId);

//comments
router.post('/comment',auth.userAuth,controller.user.addComment);
router.get('/commentByPost/:id',auth.userAuth,controller.user.getAllCommentsByPostId);
router.get('/comment/:id',auth.userAuth,controller.user.getCommentsById);
router.put('/comment/:id',auth.userAuth,controller.user.editComment);
router.delete('/comment/:id',auth.userAuth,controller.user.deleteComment);

// sendFriendRequest
router.post('/sendFriendRequest/:id',auth.userAuth,controller.user.sendFriendRequest);


module.exports=router;