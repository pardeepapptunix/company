const mongoose = require("mongoose");
const config = require('config');

const connect =async function main() {
    try {
        await mongoose.connect(config.get('mongo.url'));
        console.log("database connected");
  } catch (err) {
    console.log(err);
  }
}
module.exports=connect;